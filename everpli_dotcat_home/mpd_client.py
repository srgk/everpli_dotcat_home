##############################################################################################
# FIXME List:
# CC14 - Consider Change 14
#   Добавить опциональный параметры: логин и пароль
##############################################################################################

import logging

from mpd import MPDClient

from dpl.connections.connection import Connection
from dpl.integrations.connection_registry import ConnectionRegistry
from dpl.integrations.connection_factory import ConnectionFactory

logger = logging.getLogger(__name__)


class MPDClientConnection(MPDClient, Connection):
    def __init__(self, domain_id, host, port, timeout=10, idletimeout=None):  # CC14
        Connection.__init__(self, domain_id)
        MPDClient.__init__(self)

        self.host = host
        self.port = port
        self.timeout = timeout

        self.idletimeout = idletimeout

        # self.__test_connection()

    def __test_connection(self):
        self.connect(self.host, self.port, self.timeout)
        self.disconnect()

    def reconnect(self):
        self.connect(self.host, self.port, self.timeout)

    def __del__(self):
        self.disconnect()


class MPDClientConnectionFactory(ConnectionFactory):
    @staticmethod
    def build(*args, **kwargs) -> MPDClientConnection or None:
        try:
            return MPDClientConnection(*args, **kwargs)
        except ConnectionRefusedError as e:
            logger.warning("Unable to connect to MPD server\n%s\n%s", args, kwargs)
            raise e


ConnectionRegistry.register_factory(
    "mpd_server",
    MPDClientConnectionFactory()
)

