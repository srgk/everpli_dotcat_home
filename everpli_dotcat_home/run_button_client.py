"""
This module contains a client application which will
listen for events from buttons (connected to UART port)
and send commands to Things by the rules specified in
the legacy configuration file
"""
import argparse
import json

import serial
import requests


def main():
    """
    The main function (point of entry) of the script

    :return: None
    """
    arg_parser = argparse.ArgumentParser(
        description='everpl configuration migration utility'
    )

    arg_parser.add_argument(
        'old_config',
        help='a path to the file with button event handler settings',
        type=str

    )

    arg_parser.add_argument(
        'everpl_address',
        help='a path to the everpl API root',
        type=str
    )

    arg_parser.add_argument(
        'uart_tty',
        help='a path to the UART tty to be used',
        type=str
    )

    args = arg_parser.parse_args()

    username = input("Input the username")
    password = input("Input the password for user")

    everpl_address = args.everpl_address
    uart_tty = args.uart_tty

    with open(args.old_config) as f:
        old_config = json.load(f)

    cmd_mappings = {}

    for i in old_config:
        action = i['then']['obj_action']
        thing_id = i['then']['obj_id']

        for btn_id in i['if']['source_list']:
            cmd_mappings[btn_id] = {
                "command": action,
                "thing_id": thing_id
            }

    ##########################################################

    auth_request = requests.post(
        url=everpl_address + "/auth",
        json={
            "username": username,
            "password": password
        }
    )

    response_body = auth_request.json()

    if auth_request.status_code != 200:
        raise RuntimeError("Login failed")

    auth_token = response_body["token"]

    ##########################################################

    serial_port = serial.Serial(uart_tty, baudrate=9600)

    try:
        while 1:
            raw_data = serial_port.readline()
            print("Received: %s" % raw_data)

            button_id = raw_data.strip(b"\r\n").decode()

            execute_request_data = cmd_mappings.get(button_id)

            if execute_request_data is None:
                print("Unknown button: %s" % button_id)
                continue

            thing_id = execute_request_data['thing_id']
            command = execute_request_data['command']

            execute_response = requests.post(
                url="%s/things/%s/execute" % (everpl_address, thing_id),
                json={
                    "command": command,
                    "command_args": {}
                },
                headers={
                    "Authorization": auth_token
                }
            )

            if execute_response.status_code != 202:
                print("Failed to execute command:\n%s" % execute_response.text)

    except KeyboardInterrupt:
        pass


if __name__ == "__main__":
    main()
