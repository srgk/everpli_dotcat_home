from .shift_reg_buffered import ShiftRegBuffered

from dpl.connections.connection import Connection
from dpl.integrations.connection_factory import ConnectionFactory
from dpl.integrations.connection_registry import ConnectionRegistry
from .shift_reg_gpio import ShiftRegGPIO


class ShiftRegGPIOBuffered(ShiftRegBuffered, Connection):
    def __init__(self, domain_id, *args, **kwargs):
        Connection.__init__(self, domain_id)
        ShiftRegBuffered.__init__(
            self
            , ShiftRegGPIO(*args, **kwargs)
        )


class ShiftRegGPIOBufferedFactory(ConnectionFactory):
    @staticmethod
    def build(*args, **kwargs) -> ShiftRegGPIOBuffered:
        return ShiftRegGPIOBuffered(
            *args, **kwargs
        )


ConnectionRegistry.register_factory(
    "shift_reg",
    ShiftRegGPIOBufferedFactory()
)
