## .CAT Home integration

This repository contains an everpl integration for .CAT Home -
for the working Smart Home model created by .CAT team.

Install this package and add an "dotcat_home" string to the
list of enabled integrations in everpl configuration file to
be able to use this integration.
